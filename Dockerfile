FROM httpd:2.4
COPY ./index.html /usr/local/apache2/htdocs/
COPY ./css /user/local/apache2/htdocs/css
COPY ./imgs /usr/local/apache2/htdocs/imgs
EXPOSE 80
